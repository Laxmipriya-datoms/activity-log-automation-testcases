describe('my 3rd test suite', function(){

    it('changelog', function(){

        
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        //login to datomsx
        cy.visit('https://app.datoms.io')
        cy.get('#form_submit_btn').type('laxmipriya.puhan@datoms.io')
        cy.get('#password').type('datoms')
        cy.get('#form_submit_btn').click()

        cy.get(':nth-child(6) > a').click().should('have.text','Change Logs')

        //filter section
        cy.get('#rc_select_0').click() 
        cy.get("div div[title='Vendor User']").click({force: true})
        cy.get('.filter-select > .ant-select-selector > .ant-select-selection-item').should('have.text','Vendor User')
        cy.get('.value-select > .ant-select-selector').click()

        //verify vendor users
        cy.get('input.ant-select-selection-search-input').eq(0).click({force: true})
        cy.contains('Vendor User').click()
        cy.get('div.ant-select.value-select.ant-select-multiple.ant-select-show-search').click()
        cy.get('div.ant-select-item-option-content').each(elements=>{
            if(((elements.text()).includes('Ankur Bashir'))){
            expect(elements.text()).to.include('Ankur Bashir')
            }
            else if(((elements.text()).includes('Ashutosha Sarangi'))){
            expect(elements.text()).to.include('Ashutosha Sarangi')
            }
            else if(((elements.text()).includes('Tanjot Sethi'))){
            expect(elements.text()).to.include('Tanjot Sethi')
            }
            else if(((elements.text()).includes('Nataraj Sahoo'))){
            expect(elements.text()).to.include('Nataraj Sahoo')
            }
            else if(((elements.text()).includes('Shashant Vijay'))){
            expect(elements.text()).to.include('Shashant Vijay')
            }
            else if(((elements.text()).includes('Agniva Das'))){
            expect(elements.text()).to.include('Agniva Das')
            }

})

        //verify Application types
        cy.get('div.ant-select-selector').eq(0).click({force: true})
        cy.contains('Application Type').click({force: true})
        cy.get('div.ant-select.value-select.ant-select-multiple.ant-select-show-search').click()
        cy.get('div.ant-select-item-option-content').each(elements=>{
            if(((elements.text()).includes('Pollution Monitoring'))){
            expect(elements.text()).to.include('Pollution Monitoring')
            }
            else if(((elements.text()).includes('Energy Monitoring'))){
            expect(elements.text()).to.include('Energy Monitoring')
            }
            else if(((elements.text()).includes('Aurassure'))){
            expect(elements.text()).to.include('Aurassure')
            }
            else if(((elements.text()).includes('DG Monitoring'))){
            expect(elements.text()).to.include('DG Monitoring')
            }
            else if(((elements.text()).includes('IoT Platform'))){
            expect(elements.text()).to.include('IoT Platform')
            }
            else if(((elements.text()).includes('Delivery Tracking'))){
            expect(elements.text()).to.include('Delivery Tracking')
            }

})

        //verify entity types
        cy.get('input.ant-select-selection-search-input').eq(0).click({force: true})
        cy.contains('Entity Type').click()
        cy.get('div.ant-select.value-select.ant-select-multiple.ant-select-show-search').click()
        cy.get('div.ant-select-item-option-content').each(elements=>{
            if(((elements.text()).includes('User'))){
            expect(elements.text()).to.include('User')
            }
            else if(((elements.text()).includes('Device'))){
            expect(elements.text()).to.include('Device')
            }
            else if(((elements.text()).includes('Thing'))){
            expect(elements.text()).to.include('Thing')
            }
        })
        
        //select options from dropdown as per filter type selection
        cy.get('input.ant-select-selection-search-input').eq(0).click({force: true})
        cy.contains('Entity Type').click()
        cy.get('div.ant-select.value-select.ant-select-multiple.ant-select-show-search').click()
        cy.get("div div[title='Device']").click()
        cy.get("div div[title='Device']").then(function(elements){
            const elem2= elements.text()
            cy.wrap(elem2).as("elem22")
        })
        cy.get('span.ant-tag.ant-tag-has-color').then(function(elements){
            const elem1=elements.text()
            cy.get("@elem22").then(function (elements) {
                    const elem = elem1
                    expect(elem1).to.include(elem)
                })
        })

        cy.get("div div[title='Thing']").click()
        cy.get("div div[title='Thing']").then(function(elements){
            const elem2= elements.text()
            cy.wrap(elem2).as("elem22")
        })
        cy.get('span.ant-tag.ant-tag-has-color').then(function(elements){
            const elem1=elements.text()
            cy.get("@elem22").then(function (elements) {
                    const elem = elem1
                    expect(elem1).to.include(elem)
                })

        })

        //unselect dropdown elements
        cy.get("div div[title='Device']").click().should('not.be.selected')

        //search elements from dropdown
        cy.get('.value-select > .ant-select-selector').type('user').each(elements=>{
            if(((elements.text()).includes('User'))){
            expect(elements.text()).to.include('User')
            }
        })
        cy.get('.value-select > .ant-select-selector').type('grrbage').each(elements=>{
            if(((elements.text()).includes('No Data'))){
            expect(elements.text()).to.include('No Data')
            }
        })


        
        //select Duration
        // cy.get('div.ant-select-selector').eq(2).click()
        // cy.get('div.ant-select-selector').eq(2).then(function(elements){
        //     expect(elements.text()).to.include('This Month',{force:true})
        // })
        // cy.get('div.ant-select-selector').eq(2).click()
        // cy.get('div.rc-virtual-list-holder-inner').find('div.ant-select-item.ant-select-item-option:nth-child(3)').click()
        
        //cy.get('div.rc-virtual-list').find('span.ant-select-item-option-state').eq(2).click({force:true})


        cy.get('.rangepicker-container > .ant-select > .ant-select-selector > .ant-select-selection-item').should('have.text','This Month')
        cy.get('.rangepicker-container > .ant-select > .ant-select-selector > .ant-select-selection-item').click()
        cy.get('div.rc-virtual-list-holder-inner').each(elements=>{
            if(((elements.text()).includes('Last 3 Months'))){
                cy.get('div div[title="Last 3 Months"]').click().should('have.text','Last 3 Months')
                cy.get('div.ant-select-selector').eq(2).should('have.text','Last 3 Months')
            }
            if(((elements.text()).includes('Last 120 Days'))){
                cy.get('div div[title="Last 120 Days"]').click({force: true}).should('have.text','Last 120 Days')
                cy.get('div.ant-select-selector').eq(2).should('have.text','Last 120 Days')
            }
            
        })

        //select custom duration
        cy.get('div.ant-select-selector').eq(2)
        cy.get('div.rc-virtual-list-holder-inner')
        cy.get("div[title='Custom']").click({force: true})
        cy.get('div.ant-picker.ant-picker-range.ant-picker-default').click()
        cy.get("tr td[title='2021-03-01']").click()
        cy.get("tr td[title='2021-03-08']").click()
        //close/clear custom selected duration
        cy.get('.ant-picker-clear > .anticon > svg').click()



        // cy.get('div.ant-picker-panels').each(el=>{

        //     var dateName=el.text()
        //     if(dateName==='03'){
        //         dateName.trigger('click')
        //         }
        // })


        //pagination
        cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').should('have.text','20 / page')
        cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').click({force: true})
        cy.get('div.ant-select-item-option-content').each(elements=>{
            if(((elements.text()).includes('10 / page'))){
            expect(elements.text()).to.include('10 / page')
            cy.contains('10 / page').click()
            cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').should('have.text','10 / page')
            cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').click()
            }
            
            else if(((elements.text()).includes('50 / page'))){
                expect(elements.text()).to.include('50 / page')
                cy.contains('50 / page').click({forece:true})
                cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').should('have.text','50 / page')
                cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').click()    
            }
            else if(((elements.text()).includes('100 / page'))){
                expect(elements.text()).to.include('100 / page')
                cy.contains('100 / page').click({forece:true})
                cy.get('.ant-pagination-options > .ant-select > .ant-select-selector > .ant-select-selection-item').should('have.text','100 / page')
            }  
        })
    })
})